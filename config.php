<?php



define('_ROW_SIZE_',10);
define('_COL_SIZE_',10);

define('_STATE_ZERO_',0);
define('_STATE_ONE_',1);
define('_STATE_TWO_',2);
define('_STATE_THREE_',3);

// Cell can have one of those three status.
define('_NOT_VISITED_EMPTY',0);
define('_NOT_VISITED_FULL_',1);
define('_VISITED_EMPTY_',2);
define('_DESTROYED_',3);

define('_HAS_LEFT_NEIGHBOUR_','remleft'); // remove left border class
define('_HAS_RIGHT_NEIGHBOUR_','remright');
define('_HAS_CEILING_NEIGHBOUR_','remceiling');
define('_HAS_FLOOR_NEIGHBOUR_','remfloor');

define('_SEPERATOR_',"\\");

define('_ROOT_',dirname(__FILE__) . _SEPERATOR_);
define('_PUBLIC_ROOT_', $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME']._SEPERATOR_.$_SERVER['REQUEST_URI']);
define('_TEMPLATE_ROOT_',_ROOT_ . 'templates' .  _SEPERATOR_);

?>

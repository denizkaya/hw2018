<?php
    require_once('core.php');
    require_once('config.php');
    if ( ! session_id() ) @ session_start();
    if (isset($_POST['action']) && ($_POST['action'] == 'newGame')){
        $propertyList = [];
    } else {

        $propertyList = isset($_SESSION['batlegame'])?json_decode($_SESSION['batlegame'],true):[];
    }
    $battleGame = new Battlegame(_ROW_SIZE_, _COL_SIZE_,$propertyList);

    if (isset($_POST['action']) && ($_POST['action'] == 'newGame')){
       saveBattleField($battleGame);
       $content = newGameContent($battleGame, $view);
       $result = array_merge(['message' => 'New game started', 'content' => $content], $battleGame->getShipsStatus());
       exit(json_encode($result));
    } else if (isset($_POST['action']) && ($_POST['action'] == 'pointPressed')){
        $result = checkPressedCoordinate($battleGame,$_POST['coordinate']);
        $result['content'] = newGameContent($battleGame, $view);
        saveBattleField($battleGame);
        $result = array_merge($result, $battleGame->getShipsStatus());
        exit(json_encode($result));
    } else {
      $content = newGameContent($battleGame, $view);
    }

    $view->content = $content;
    echo $view->render('Main.php');

   function checkPressedCoordinate(&$battleGame,$coordinate){
      $coordinates = explode("_",$coordinate);
      $coordinate = ['x' => $coordinates[1], 'y'=> $coordinates[0]];
      return $battleGame->evaluatePressedCell($coordinate);
   }
   function saveBattleField($battleGame){
       $result = $battleGame->prepareToSave();
       $_SESSION['batlegame'] = json_encode($result);
   }

   function newGameContent(&$battleGame, &$view){
        $view = new Template();
        $view->title = "Welcome to batle field!";
        $view->battleGame = $battleGame;
        return $view->render('Content.php');
    }

  <table>
    <?php foreach($battleGame->battleFieldMatrix as $rkey =>$rows): ?>
      <tr>
        <?php foreach($rows as $ckey => $col): ?>
          <?php  $id = $rkey . "_" . $ckey;
                $tag = '';
                if ($col == 2){
                    $tag = "visited";
                }
                else if ($col == 3){
                    $tag = "destroyed";
                    if (isset($battleGame->shipBorderInsides[$id])){
                        $tag = $tag . ' '. $battleGame->shipBorderInsides[$id];
                    }
                }
          ?>
          <td id="<?php echo $id; ?>" class="<?php echo $tag; ?>"></td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </table>

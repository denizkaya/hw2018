<html>
<head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/control.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/battleship.css">

    <title><?php echo $title; ?></title>
</head>
<body data-url="<?php echo _PUBLIC_ROOT_; ?>">
    <h1><?php echo $title; ?></h1>
    <div id="main">
      <div id="board">
      <div style="float:left; width:10%;"> </div>
      <div style="float:left; width:50%; ">
      <div id="tableContent">
        <?php echo $content; ?>
      </div>
      </div>
      <div style="float:left; width:40%; ">
         <div style="float:left; height:8%; width:100%; ">
           <input type="button" id="loadButton" value="New Game" class="btn" style="width:100%;height:100%"/>
         </div>
         <div style="float:left; height:12%; width:100%;">
           <div style="float:left; height:100%; width:80%; padding-top: 8px; ">
               <span class="info">Remained Ship</span>
               <span class="info">Destroyed Ship</span>
          </div>
          <div style="float:left; height:100%; width:5%; padding-top: 8px;">
              <span class="info">:</span>
              <span class="info">:</span>
         </div>
         <div style="float:left; height:100%; width:15%; padding-top: 8px;">
             <span class="info" id="shipRemained">4</span>
             <span class="info" id="shipDestroyed">0</span>
        </div>
         </div>
         <div style="float:left; height:100%; width:100%; padding-top: 8px">
               <span class="info" id="messageBox">Message Box.</span>
         </div>
      </div>
      </div>

    </div>
  </body>
</html>

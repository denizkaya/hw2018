$( document ).ready(function() {
    $(document).on('click', '#loadButton', function () {
    // $('#loadButton').click(function() {
         $.ajax({ url: $('body').data('url')+'/index.php',
               data: {action: 'newGame'},
               type: 'post',
               dataType: "json",
               success: function(output) {
                   setResult(output);
              }
      });
  });

  $(document).on('click', 'td', function () {
        if ($(this).hasClass("visited") || $(this).hasClass("destroyed")){
            $("#messageBox").html("Have already visited this cell.");
            return false;
        }
        $("#messageBox").html();
        var id = $(this).attr('id');
        $.ajax({ url: $('body').data('url')+'/index.php',
                 data: {action: 'pointPressed', coordinate:id},
                 type: 'post',
                 dataType: "json",
                 success: function(output) {
                    setResult(output);
                }
        });
    });
    function setResult(output){
      $( "#tableContent" ).html(output['content']);
      $("#shipRemained").html(output['shipRemained']);
      $("#shipDestroyed").html(output['shipDestroyed']);
      $("#messageBox").html(output['message']);

      if (output['shipRemained'] == 0 ){
          alert('Congratulations. You finished the game!');
          makeCelsVisited();
      }

    }
    function makeCelsVisited(){
      $("table").find('td').each (function() {
          if (($(this).hasClass("visited") == false) && ($(this).hasClass("destroyed") == false)){
              $(this).addClass("visited");
          }
      });
    }
});

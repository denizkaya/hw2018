<?php

abstract class Ship
{

    private $referans_point;
    public $name;
    public $isDestroyed;
    public $coordinates;
    public $referantialCoordinates;
    public $hitlist;
    public $borderInsides;

    public function __construct($name,$propertyList){

        if (empty($propertyList)){
            $this->name = $name;
            $this->referans_point = ['x'=>0, 'y'=>0];
            $this->isDestroyed = 0;
            $this->borderInsides = [];
        } else {
            $this->loadFromPropertyList($propertyList);
        }
    }

    private function loadFromPropertyList($propertyList){
        $this->name = $propertyList['name'];
        $this->coordinates = $propertyList['coordinates'];
        $this->isDestroyed = $propertyList['isDestroyed'];
        $this->hitList = $propertyList['hitList'];
        // $this->borderInsides
    }

    public function realLocation($referans_point, $state){
       $result = [];
       $index = 0;
       foreach ($this->referantialCoordinates as $key => $coordinate) {
          switch ($state) {
          case _STATE_ZERO_: // no rotation
                $result[$index] = [ 'x' =>  $referans_point ['x'] + $coordinate['x'],
                                      'y' =>  $referans_point ['y'] + $coordinate['y'],
                                    ];
                 break;
          case _STATE_ONE_: // 90 degree clockwise
                $result[$index] = [ 'x' =>  $referans_point ['x'] + (-1 * $coordinate['y']),
                                      'y' =>  $referans_point ['y'] + $coordinate['x'],
                                    ];
                break;
          case _STATE_TWO_: // 180 degree clockwise
                $result[$index] = [ 'x' =>  $referans_point ['x'] + (-1 * $coordinate['x']),
                                      'y' =>  $referans_point ['y'] + (-1 * $coordinate['y']),
                                    ];
                break;
          case _STATE_THREE_: // 270 Degree clockwise
                $result[$index] = [ 'x' =>  $referans_point ['x'] + $coordinate['y'],
                                      'y' =>  $referans_point ['y'] + (-1 * $coordinate['x']),
                                    ];
                break;
         }
         if ( $result[$index]['x']< 0 ||
              $result[$index]['y']< 0 ||
              $result[$index]['x']>= _COL_SIZE_ ||
              $result[$index]['y']>= _ROW_SIZE_ ) {
              return false;
         }
         $index = $index + 1;

       }
       return $result;
    }

    public function prepareToSave(){
       $result = [ 'name' => $this->name,
                   'coordinates' => $this->coordinates,
                   'isDestroyed' => $this->isDestroyed,
                   'hitList' => $this->hitList,
                   'shipClass' => get_class($this),
       ];
       return $result;
    }

    public function setReferantialCoordinates($referantialCoordinates){

        $this->referantialCoordinates = $referantialCoordinates;
        //hit list needs to be emtptied
        $this->hitList = array_fill(0, sizeof($referantialCoordinates),0);
    }
    public function checkCoordinate($coordinateToBeChecked){
       $result = false;
       foreach ($this->coordinates as $key => $coordinate) {
          if ( $coordinateToBeChecked['x'] == $coordinate['x'] &&
               $coordinateToBeChecked['y'] == $coordinate['y']){
              $this->hitList[$key] = 1;
              $result = true;
              $this->isDestroyed = !in_array(false,$this->hitList);
          }
       }
       return $result;
    }

    /*  Check whether the coordinate in ships coordinate. */
    public function isCoordinateInShipLocations($x,$y){
        if ($x < 0 || $y < 0 || $x > _COL_SIZE_-1 || $y > _ROW_SIZE_-1){
            return false;
        }
        foreach ($this->coordinates as $key => $coordinate) {
            if ( $coordinate['x'] == $x &&  $coordinate['y'] == $y){
                return true;
            }
        }
        return false;
    }
    /*  Sets the borders which is inside for the ship coordinates. */
    public function setBorderInsides(){
      foreach ($this->coordinates as $key => $coordinate) {
          $x = $coordinate['x'];
          $y = $coordinate['y'];
          if ($this->isCoordinateInShipLocations($x-1,$y)){
              $this->borderInsides[$y.'_'.$x][] = _HAS_LEFT_NEIGHBOUR_;
          }
          if ($this->isCoordinateInShipLocations($x+1,$y)){
              $this->borderInsides[$y.'_'.$x][] = _HAS_RIGHT_NEIGHBOUR_;
          }
          if ($this->isCoordinateInShipLocations($x,$y-1)){
              $this->borderInsides[$y.'_'.$x][] = _HAS_CEILING_NEIGHBOUR_;
          }
          if ($this->isCoordinateInShipLocations($x,$y+1)){
              $this->borderInsides[$y.'_'.$x][] = _HAS_FLOOR_NEIGHBOUR_;
          }
      }
    }
    public function getLocation(){
      return $coordinates;
    }
    public function setLocation($coordinates){
      $this->coordinates = $coordinates;
      // Set the inside borders ...
      $this->setBorderInsides();
    }
}

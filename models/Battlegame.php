<?php

class Battlegame {
  public $battleFieldMatrix;
  public $shipList;
  public $shipBorderInsides;
  public function __construct($rowSize, $columnSize, $propertyList){
      $this->initMatrix($rowSize, $columnSize);
      $this->shipList = [];
      $this->shipBorderInsides = [];
      if (empty($propertyList)){
          $this->addShipToBattleField(new Lship("ship1"));
          $this->addShipToBattleField(new Iship("ship2"));
          $this->addShipToBattleField(new Onedotship("ship3"));
          $this->addShipToBattleField(new Onedotship("ship4"));
          $this->setBorderInsideOfShips();
      } else{
          $this->battleFieldMatrix = $propertyList['matrix'];
          $this->shipBorderInsides = $propertyList['borderInsides'];
          foreach ($propertyList['ships'] as $key => $ship) {
            $this->shipList [] = new $ship['shipClass']($ship['name'],$ship);
          }
      }

  }
  private function setBorderInsideOfShips(){
    $borderInsides = [];
    foreach ($this->shipList as $key => $ship) {
       $borderInsides = array_merge($borderInsides, $ship->borderInsides);
    }
    foreach ($borderInsides as $key => $borderArray) {
       $this->shipBorderInsides[$key] = implode(" ",$borderArray);
    }
  }


  private function initMatrix($row_count, $col_count){
      $this->battleFieldMatrix = [];
      for ($i=0; $i <$row_count ; $i++) {
          for ($j=0; $j <$col_count ; $j++) {
              $this->battleFieldMatrix[$i][$j] = _NOT_VISITED_EMPTY;
          }
      }
  }
  /*
    It checks whether another ship near. If it is adjacent return false.
  */
  public function boundryControl($candidateLocation){
      foreach ($candidateLocation as $key => $coordinate) {
          for ($i=0; $i<3; $i++){
            $from_x = $coordinate['x']-1 + $i;
            if ($from_x >= 0 && $from_x < _COL_SIZE_){
              for ($j=0; $j<3; $j++){
                  $from_y = $coordinate['y']-1 + $j;
                  if ($from_y >= 0 && $from_y < _ROW_SIZE_ &&
                     $this->battleFieldMatrix[$from_y][$from_x] == _NOT_VISITED_FULL_){
                     return false;
                  }
              }
            }
          }
      }
      return true;
  }

  public function getPoints($order){
      $x = fmod($order, _COL_SIZE_);
      $y = floor($order / _ROW_SIZE_);
      return ['x' => $x, 'y'=> $y];
  }

  public function getRandomisedOrder($size){

      $random_array = [];
      for ($i=0; $i<$size; $i++){
          $random_array[] = $i;
      }
      shuffle($random_array);
      return $random_array;
  }

  public function addShipToBattleField($ship){
    $candidate_locations = $this->getRandomisedOrder(_ROW_SIZE_ * _COL_SIZE_);
    if (get_class($ship) == 'Onedotship'){
        $status_orders = array(0);
    } else {
        $status_orders =  $this->getRandomisedOrder(4);
    }


    foreach ($candidate_locations as $key => $value) {
        $reference_point = $this->getPoints($value);
        foreach ($status_orders as $key => $status) {
            $candidateLocation = $ship->realLocation($reference_point, $status);
            if ($candidateLocation !== false && $this->boundryControl($candidateLocation)) {
                $ship->setLocation($candidateLocation);
                $this->shipList[] = $ship;
                foreach ($candidateLocation as $key => $coordinate) {
                    $this->battleFieldMatrix[$coordinate['y']][$coordinate['x']] = _NOT_VISITED_FULL_;
                }
                return true;
            }
        }
    }
      return false;
  }
  public function prepareToSave(){
      $ships = [];
      foreach ($this->shipList as $key => $ship) {
          $ships[] =  $ship->prepareToSave();
      }

      $result = ['matrix' => $this->battleFieldMatrix,
                 'borderInsides' => $this->shipBorderInsides,
                 'ships' => $ships,
      ];
      return $result;
  }

  public function evaluatePressedCell($coordinate){

      $x = $coordinate['x'];
      $y = $coordinate['y'];
      if ($x < 0 || $x > _COL_SIZE_-1 ||
          $x < 0 || $y > _ROW_SIZE_-1) {
          return ['message' => 'The coordinate is out of boundry!'];
      }

      if ($this->battleFieldMatrix[$y][$x] > _NOT_VISITED_FULL_) {
          return ['message' => 'The coordinate was checked before!'];
      }

      $cellIsBelongsAShip = false;
      foreach ($this->shipList as $key => $ship) {
          if (!$ship->isDestroyed){
              if ($ship->checkCoordinate($coordinate)){
                 $this->battleFieldMatrix[$y][$x] = _DESTROYED_;
                 $cellIsBelongsAShip = true;
                 $msg = 'Hit a ship cell! ';
              }
          }
      }

      if (!$cellIsBelongsAShip){
         $this->battleFieldMatrix[$y][$x] = _VISITED_EMPTY_;
         $msg = 'Hit an empty cell! ';
      }
      return array_merge(['message' => $msg]);
  }

  public function getShipsStatus(){
      $shipsDestroyed = 0;
      foreach ($this->shipList as $key => $ship) {
          if ($ship->isDestroyed){
              $shipsDestroyed = $shipsDestroyed + 1;
          }
      }
      return ['shipDestroyed' => $shipsDestroyed, 'shipRemained' => sizeof($this->shipList) - $shipsDestroyed];
  }


}

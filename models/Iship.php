<?php
class Iship extends Ship {

  public function __construct($name,$propertyList = []){
      $referantialCoordinates = [ ['x'=>0, 'y'=>0],
        ['x'=>0, 'y'=>1],
        ['x'=>0, 'y'=>2],
        ['x'=>0, 'y'=>3],
      ];
      $this->setReferantialCoordinates($referantialCoordinates);
      parent::__construct($name,$propertyList);
  }
}

<?php
class Twodotship extends Ship {

  public function __construct($name,$propertyList = []){
      $referantialCoordinates = [ ['x'=>0, 'y'=>0],
        ['x'=>0, 'y'=>1],
      ];
      $this->setReferantialCoordinates($referantialCoordinates);

      parent::__construct($name,$propertyList);
  }
}
